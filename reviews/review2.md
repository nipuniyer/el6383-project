
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.

Answer: By studying how to control the flow of traffic on a router and how to adjust the queue size of a router the experiment designers intend
        to measure the impact of varying the queue size on delay and packet loss using UDP traffic.

2) Does the project generally follow the guidelines and parameters we have
learned in class?

Answer: The project follows most of the guidelines but not completely. It's an experiment based on validation but not on exploration



## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

Answer: The goal of this experiment is to measure the impact of varying the queue size on delay and packet loss using UDP traffic. It is focused
        and spesific goal. I believe it's useful as upcomeings like voice over IP and video streaming are based on UDP.


2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?

Answer: Yes, the metrics and parameter were chosen appropriately and the experiment design clearlt supports the experiment goal.

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?

Answer: No, the experiment is not well designed to obtain maximum information with the minimum number of trials as it's a two node topology and
        It doesn't account for in case of failure of a node or a link.


4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?

Answer: Yes, the mtrics were selected approprately and they are clear.


5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?

Answer: The parameter is meaningful but he range is not representative, I belive in practical the queue size range will be greater than what the
        experimenters have considered.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?

Answer: The author addresss the possibility of intercations. They said There is only one parameter which is going to be varied,
        hence they dont expect any interaction



7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?

Answer: There are no comparision in this experiment.



## Communicating results


1) Do the authors report the quantitative results of their experiment?

Answer: Yes, the author report the quantitative results of their experiment.

2) Is there information given about the variation and/or distribution of
experimental results?

Answer: yes, the information was given


3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?

Answer: Yes, the author practiced data integrity if they got those results that they have submitted.


4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?


5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?

Answer: The data is presented in a clear and effective way and the graphical form that they choosen was the effective one.

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

Answer: No, the author doesn't include instructions for reproducong experiment in 3 ways. The instructions that they have given are clear and
        easy to understand up to certain point, but the instructions for data visualization are not clear

2) Were you able to successfully produce experiment results?

Answer: Yes, I was successfully able to reproduce the experiment and it's data. But I was unable to visualize them. The ".py" file that they have
        given to get numerical data from the ".csv" files but they are giving charcter output. Those files were in submit folder under the message
		"text format files"


3) How long did it take you to run this experiment, from start to finish?

Anser: It took me one day to run the experiment from start to finish


4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

Answer: I didn't make any changes to the instructions that author were given, but I used my own ".sh" files that I used to get numerical data
        in my own experiment to get the numerical data for this experiment and that too didn't work.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

Answer: I falls on to the third degree among the six degrees of reproducinility
## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.

I belive that ".py" that they used to get numerical data from the files is downloaded form internet. But I would say that you better write your own
".py" files to get numerical data.
One more thing is they saved their ouput files in .csv format then converted them to .txt format and from them they obtained numerical data to visualize
I would say they can simply save in .txt fromat and do the rest

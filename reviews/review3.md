
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.

	This experiment measures the impact of varying the queue size on delay and packet loss using UDP traffic
	by setting two end host with a intermediate router, in which queue size will be adjusted.

2) Does the project generally follow the guidelines and parameters we have
learned in class?

	Yes, basiclly it follows the guidelines and parameters. The goal is clear.
	But there is only one parameter in the experiment.

## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

	The goal of this experiment is to measure the impact of varying the queue size on delay and packet loss using UDP traffic.
	it is a focused, specific goal. It is a basic expeiment and it is useful to understand how the queue size works.

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?

	The metrics are the maximum	delay and packet lost(by number and by percentage). The parameter is the queue size.
	They basiclly appropriate for this experiment goal. But the maximum delay should not be the only metric to test delay,
	average delay should also be considerded.

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?

	Yes, there are 6 trails in the experiment to get information of both maximum delay and packet loss. It is well designed.

4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?

	As I said in question (2), information of average delay and other analysis can also metrics since the goal of this experiment
	contains the impact on delay, not just the Maximun delay.

5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?

	The parameters of the experiment is meaningful, because the queue size is match to the goal.
	The ranges over the queue size is from 0.1Mb to 3Mb. However, the author didn't declear the reason to set this range.
	I think it might be meaningful and representative, because the result should be the same if we keep increasing the queue size.
	The delay would go up and the packet loss would go down.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?

	There is no interactions between parameters in this experiment.


7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?  

	Comparisons are the difference on the maximun delay and the packet loss.
	Comparisons are made reasonably, because the baseline is exactly the same, the difference comes from the different queue size.


## Communicating results


1) Do the authors report the quantitative results of their experiment?

	Yes, the authors report the quantitative results of their experiment by obsereving the ping output.
	They explain the reason why intially the RTT keeps on increasing and drops suddenly..

2) Is there information given about the variation and/or distribution of
experimental results?

	There is information about the variation, the authors explained how it works in conclusion.
	But no information about the distribution.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?

	Yes,  the authors practice data integrity.

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?

	Yes, the data presented in graphical form, which is very clear and effective.
	Yes, the type is right. They choose line chart to tell the story, which is good to explain variation.

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?

	Yes, the conclusion is clearly supported by the experiment results. The author explained the results in details.

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

	Yes, the authors include instructions for reproducing the experiment in the way of Existing experiment setup -> Data.
	Yes, the instructions are clear and easy to understand.

2) Were you able to successfully produce experiment results?

	Yes, I were able to reproduce the experiment results.

3) How long did it take you to run this experiment, from start to finish?

	 it took mea bout 30 min to run this experiment.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

	No, all of steps are given by the documentation.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

	it falls on the 4th degree of reproducibility.


## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.

	Good job.

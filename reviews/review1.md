
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.

	This project studies about the data traffic flow control over a router by adjusting queue sizes. This experiment in this project
	deals with measuring loss and delay with change in queue sizes by generating UDP traffic between a source and a destination connected
	by an intermediate router.


2) Does the project generally follow the guidelines and parameters we have
learned in class?

	Most of the time this project follows the guidelines and parametersds that we have learned in the class.



## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

	Goal of this experiment is exploring the effects of queues on packet loss and delay using UDP traffic. Overall it is a specific
	goal likely to have known results.

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?

	The metrics and parameters chosen are appropriate for the experiment goal with the design supporting the experiment goal.

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?

	This experiment is likely to obtain the information with minimum number of trials.

4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?

	The metrics used are right and also changes in packet burst values should have been used to see the changes in loss and delay
	for the UDP traffic.

5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?

	The ranges over which parameters vary are meaningful and representative.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?

	The authors should have taken more variations in parameters while throttling the router.


7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?

	The baseline selected for comparison is appropriate and realistic.


## Communicating results


1) Do the authors report the quantitative results of their experiment?

	The authors have reported the quantitative results of their experiments in .csv files but are not being tabulated for
	comparisons.

2) Is there information given about the variation and/or distribution of
experimental results?

	Information about variations in experimental results is graphically represented.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?

	The authors have maintained data integrity.

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?

	The data presented in graphical form is lucid and inaccordance with the data.

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?

	The conclusions drawn by the author and their goal are not suffciently supported by the experimental results.


## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

	They have mentioned the instructions for reproducing the experiment in all 3 ways and the instructions are clear.

2) Were you able to successfully produce experiment results?

	I am able to successfully produce the experiment results.

3) How long did it take you to run this experiment, from start to finish?

	It took 25mins to run the experiment from start to finish.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

	I did not make any changes or do additional steps beyond the documentaion.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

	It falls under this degree:
	The results can be easily reproduced by an independent researcher with at most 15 mins of user effort requiring onlt standard,
	freely available tools.



## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.

	To impove this experiment they should have also shown changes in delay and loss for different queue sizes not only by
	changing the bandwidth but also should have taken into consideration for changes packet burst values of the UDP traffic.
